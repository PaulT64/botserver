


public interface MotorDriver {
    void backward();
    void forward();
    void left();
    void right();
    void stop();
    void restart();
}

