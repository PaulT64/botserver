import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataListener;
import com.pi4j.io.serial.SerialFactory;

public class AMega implements MotorDriver {
    private final Serial serial;
    private String distance;
    public AMega(){

        serial = SerialFactory.createInstance();
        String port;
        //port = "/dev/ttyUSB0"
        port = "/dev/ttyACM0";
        serial.open(port, 115200);

        serial.addListener(new Listener());
        
    }

    @Override
    public synchronized void forward(){
        serial.write((byte) 'w');
        serial.flush();
    }
    
    @Override
    public synchronized void left() {
        serial.write((byte) 'a');
        serial.flush();
    }
    
    @Override
    public synchronized void right () {
        serial.write((byte) 'd');
        serial.flush();
    }

    @Override
    public synchronized void backward () {
        serial.write((byte) 's');
        serial.flush();
    }

    @Override
    public synchronized void stop () {
        serial.write((byte) 'x');
        serial.flush();
    }

    @Override
    public synchronized void restart(){
        serial.write((byte) 'r');
        serial.flush();
    }

    private class Listener implements SerialDataListener {

        @Override
        public void dataReceived(SerialDataEvent event) {
            synchronized(AMega.this){
                distance = event.getData();
                //System.out.println(event.getData());
            }
        }
    }

}
