public class FakeMotorDriver implements MotorDriver {
    @Override
    public void backward() {
        System.out.println("Движение назад.");
    }
    @Override
    public void forward() {
        System.out.println("Движение вперед.");
    }
    @Override
    public void left() {
        System.out.println("Движение налево.");
    }
    @Override
    public void right() {
        System.out.println("Движение направо.");
    }
    @Override
    public void stop() {
        System.out.println("Остановка.");
    }
    @Override
    public void restart() {
        System.out.println("Перезапуск управляющей платы.");
    }
}
