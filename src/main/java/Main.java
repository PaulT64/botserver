import fi.iki.elonen.NanoHTTPD;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;


public class Main extends NanoHTTPD {

    // Содержимое файла index.html
    private final String INDEX_HTML = readResource("index.xhtml");
    // Содержимое файла style.css
    private final String STYLE_CSS = readResource("style.css");

    // Драйвер двигателя
    private final MotorDriver md;

    private String readResource(String resourceName){
        InputStream inputStream = getClass().getResourceAsStream(resourceName);

        try {
            final char[] buffer = new char[1024];
            final StringBuilder out = new StringBuilder();

            Reader in = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            for (;;) {
                int rsz = in.read(buffer, 0, buffer.length);
                if (rsz < 0) break;
                out.append(buffer, 0, rsz);
            }

            return out.toString();
        } catch (IOException ex) {
            return null;
        }

    }

    public static void main(String[] args) throws InterruptedException {

        // Здесь определеяется какой драйвер двигателя будет использован
        // Если передан параметр debug, то создается эмуляция.
        MotorDriver md;
        if(args.length > 0 && "debug".equals(args[0])){
            md = new FakeMotorDriver();
        } else {
            md = new AMega();
        }

        // Создается и запускается http-сервер.
        Main afelBotMain;
        try {
            afelBotMain = new Main(md);
            afelBotMain.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        } catch (IOException ioe) {
            System.err.println("ERROR Starting server:\n" + ioe);
        }

        md.stop();
    }

    public Main(MotorDriver md) throws IOException{
        // сервер создается на порту 8000.
        super(8000);

        this.md = md;
        System.out.println("!!! Server started !!!");
    }


    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();

        Response newFixedLengthResponse;
        switch(uri){
            case "/":
            case "/index.html":

                String button = session.getParms().get("button");
                if(button != null){
                    // Обработка нажатой кнопки
                    switch (button) {
                        case "forward":
                            md.forward();
                            break;
                        case "turnleft":
                            md.left();
                            break;
                        case "turnright":
                            md.right();
                            break;
                        case "backward":
                            md.backward();
                            break;
                        case "restart":
                            md.restart();
                            break;
                        case "stop":
                        default:
                            md.stop();
                            break;
                    }
                }

                // Возвращаю содержимое страницы
                newFixedLengthResponse = newFixedLengthResponse(INDEX_HTML);
                newFixedLengthResponse.setMimeType(MIME_HTML);
                return newFixedLengthResponse;

            case "/style.css":

                // Здесь возвращается файл стилей.
                newFixedLengthResponse = newFixedLengthResponse(STYLE_CSS);
                newFixedLengthResponse.setMimeType("text/css");
                return newFixedLengthResponse;

            default:
                // Выдаст ошибку 404, файл не найден.
                return super.serve(session);
        }

    }

}
